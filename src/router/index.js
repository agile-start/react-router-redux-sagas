import React from 'react';
import { Route, Switch } from 'react-router-dom';

import ScreenApod from '../screens/apod';

const preRender = (Element, { match }) => {
  return <Element match={match} />;
};

const routes = [
  {
    key: 'root',
    path: '/',
    render: context => preRender(ScreenApod, context),
    exact: true,
    effect: 'fade',
  },
  {
    key: 'date',
    path: '/:date',
    render: context => preRender(ScreenApod, context),
    exact: true,
    effect: 'swipe',
  },
];

export default () => (
  <Switch>
    {routes.map(route => <Route {...route} />)}
  </Switch>
);
