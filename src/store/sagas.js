import { all } from 'redux-saga/effects';

import apod from './apod/sagas';

export default function* rootSaga () {
  yield all([
    ...apod,
  ]);
}
