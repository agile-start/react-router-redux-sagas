import { UPDATE_APOD_DATE, UPDATE_APOD_MEDIA, INVALID_APOD_DATE } from './actions';

// Default value to start
const defaultState = {
  date: null,
  status: 'ok',
  media: {
    copyright: '',
    explanation: '',
    media_type: '',
    title: '',
    url: '',
  },
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case UPDATE_APOD_DATE:
      return { ...state, status: 'ok', date: action.payload };
    case UPDATE_APOD_MEDIA:
      return { ...state, status: 'ok', media: action.payload };
    case INVALID_APOD_DATE:
      return { ...state, status: 'error', media: defaultState.media };
    default:
      return state;
  }
};
