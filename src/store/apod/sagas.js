import { takeEvery, put, select } from 'redux-saga/effects';

import { apod } from '../../library/nasa-api';

import {
  LOAD_APOD, updateApodDate, updateApodMedia, invalidApodDate,
} from './actions';
import { startLoading, stopLoading } from '../loading/actions';

function* handleNewMessage () {
  yield takeEvery(LOAD_APOD, function* callback (action) {
    const store = yield select();

    if (action.payload !== store.apod.date) {
      yield put(startLoading());
      yield put(updateApodDate({ date: action.payload }));

      try {
        const data = yield apod({ date: action.payload }).then(response => response);
        yield put(updateApodMedia(data));
      } catch (error) {
        yield put(invalidApodDate());
      }

      yield put(stopLoading());
    }
  });
}

export default [
  handleNewMessage(),
];
