export const LOAD_APOD = 'LOAD_APOD';
export const UPDATE_APOD_DATE = 'UPDATE_APOD_DATE';
export const UPDATE_APOD_MEDIA = 'UPDATE_APOD_MEDIA';
export const INVALID_APOD_DATE = 'INVALID_APOD_DATE';


export function loadApod (value) {
  return {
    type: LOAD_APOD,
    payload: value.date,
  };
}

export function updateApodDate (value) {
  return ({
    type: UPDATE_APOD_DATE,
    payload: value.date,
  });
}

export function updateApodMedia (value) {
  return ({
    type: UPDATE_APOD_MEDIA,
    payload: value,
  });
}

export function invalidApodDate () {
  return {
    type: INVALID_APOD_DATE,
  };
}
