import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { loadApod } from '../../store/apod/actions';

import Apod from '../../components/apod';
import Navigator from '../../components/navigator';

import './style.scss';

const ScreenApod = ({
  apod, apodStatus, load, match,
}) => {
  const { date } = match.params;

  load({ date });

  return (
    <div className="screen-apod">
      <Navigator date={date} />
      {
        apodStatus === 'ok'
          ? <Apod data={apod} />
          : <div className="screen-apod__erro">Content not found for the date</div>
      }
    </div>
  );
};

ScreenApod.propTypes = {
  apod: PropTypes.object.isRequired,
  apodStatus: PropTypes.string.isRequired,
  load: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  apod: state.apod.media,
  apodStatus: state.apod.status,
});

const mapDispatchToProps = dispatch => ({
  load: params => dispatch(loadApod(params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ScreenApod);
