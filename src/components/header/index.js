import React from 'react';

import './style.scss';

const Header = () => (
  <div className="component-header">
    <h1>Astronomy Picture of the Day</h1>
  </div>
);

export default Header;
