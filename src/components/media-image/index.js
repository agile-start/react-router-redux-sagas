import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

const MediaImage = ({
  url, copyright, className,
}) => {
  return (
    <figure className={`component-media-image ${className}`}>
      <img src={url} alt="Of the day" />
      <figcaption>
          Copyright: {copyright}
      </figcaption>
    </figure>
  );
};

MediaImage.propTypes = {
  url: PropTypes.string.isRequired,
  copyright: PropTypes.string,
  className: PropTypes.string,
};

MediaImage.defaultProps = {
  copyright: '',
  className: '',
};

export default MediaImage;
