import axios from 'axios';

import env from '../../config/env';

export function apod ({ date }) {
  let apiUrl = `${env.REACT_APP_NASA_API_HOST}planetary/apod?api_key=${env.REACT_APP_NASA_API_KEY}`;

  if (date) apiUrl += `&date=${date}`;

  console.log('aqui 2');

  return axios.get(apiUrl)
    .then((dataRequest) => {
      console.log(dataRequest);
      return dataRequest.data;
    });
}

export default {
  apod,
};

// REACT_APP_NASA_API_KEY=FOzEua7wkbdc7sBCEDFoFAPL9bvg3ff1wEE6JazZ
