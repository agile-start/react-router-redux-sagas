import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import Store from './store';
import Page from './pages/main';

import registerServiceWorker from './registerServiceWorker';

render(
  <Store>
    <BrowserRouter>
      <Page />
    </BrowserRouter>
  </Store>,

  document.getElementById('root'),
);

registerServiceWorker();
