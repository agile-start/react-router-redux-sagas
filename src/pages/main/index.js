import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Header from '../../components/header';
import Footer from '../../components/footer';
import Loading from '../../components/loading';
import Router from '../../router';

import './style.scss';

const PageMain = ({ loading }) => (
  <div className="page-main">
    <Header />
    {
      loading
        ? <Loading className="page-main__loading" />
        : <Router />
    }
    <Footer />
  </div>
);

PageMain.propTypes = {
  loading: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  loading: state.loading,
});

export default connect(mapStateToProps)(PageMain);
